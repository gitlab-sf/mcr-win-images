Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"
$InformationPreference = "Continue"
# ---------------------------------------------------------------------------
# This script depends on a few environment variables that should be populated
# before running the script:
#
# - $Env:WINDOWS_VERSION - This is the version of windows that is going to be
#   used for building the Docker image. It is important for the version to match
#   one of the mcr.microsoft.com/powershell or https://hub.docker.com/_/microsoft-powershell
#   For example, `windowsservercore-20h2` will build from mcr.microsoft.com/powershell:windowsservercore-20h2.
# - $Env:GIT_VERSION - Specify which version of Git needs to be installed on
#   the Docker image. This is done through Docker build args.
# - $Env:GIT_VERSION_BUILD - Specify which build is needed to download for the
#   GIT_VERSION you specified.
# - $Env:GIT_WINDOWS_AMD64_CHECKSUM - The checksum of the downloaded zip, usually found in
#   the GitHub release page.
# - $Env:GIT_LFS_VERSION - The Git LFS version needed to install on the
#   Docker image.
# - $Env:GIT_LFS_WINDOWS_AMD64_CHECKSUM - The checksum of the downloaded .tar.gz file, usually
#   found in the GitHub release page.
# - $Env:PWSH_VERSION - The Powershell Core version needed to install on the
#   Docker image.
# - $Env:CI_REGISTRY_IMAGE - Image name to push to GitLab registry. Usually set
#   by CI.
# - $Env:CI_REGISTRY - The GitLab registry name. Usually set by CI.
# - $Env:CI_REGISTRY_USER - The user used to login CI_REGISTRY. Usually set by
#   CI.
# - $Env:CI_REGISTRY_PASSWORD - The password used to login CI_REGISTRY. Usually
#   set by CI.
# - $Env:DOCKERHUB_USER - The user used to login docker.io
# - $Env:DOCKERHUB_PASSWORD - The password used to login docker.io
# - $Env:DOCKERHUB_IMAGE - The dockerhub registry name
# ---------------------------------------------------------------------------
function Main
{
    Build-Image $Env:WINDOWS_VERSION

    Connect-Registry "$Env:CI_REGISTRY_USER" "$Env:CI_REGISTRY_PASSWORD" "$Env:CI_REGISTRY"
    Push-Tag "$Env:CI_REGISTRY_IMAGE/gitlab-runner-helper:$Env:WINDOWS_VERSION"
    Disconnect-Registry "$env:CI_REGISTRY"

    Connect-Registry "$Env:DOCKERHUB_USER" "$Env:DOCKERHUB_PASSWORD" "docker.io"
    Push-Tag "${Env:DOCKERHUB_IMAGE}:${Env:WINDOWS_VERSION}"
    Disconnect-Registry "docker.io"
}


function Build-Image($tag)
{
    Write-Information "Build image for $tag"

    $buildArgs = @(
        '--build-arg', "BASE_IMAGE_TAG=mcr.microsoft.com/powershell:$tag",
        '--build-arg', "GITLAB_RUNNER_WINDOWS=$Env:GITLAB_RUNNER_WINDOWS",
        '--build-arg', "GIT_VERSION=$Env:GIT_VERSION",
        '--build-arg', "GIT_VERSION_BUILD=$Env:GIT_VERSION_BUILD",
        '--build-arg', "GIT_AMD64_CHECKSUM=$Env:GIT_AMD64_CHECKSUM"
        '--build-arg', "GIT_LFS_VERSION=$Env:GIT_LFS_VERSION"
        '--build-arg', "GIT_LFS_AMD64_CHECKSUM=$Env:GIT_LFS_AMD64_CHECKSUM"
    )

    $imageNames = @(
        '-t', "${Env:CI_REGISTRY_IMAGE}/gitlab-runner-helper:$tag",
        '-t', "${Env:DOCKERHUB_IMAGE}:$tag"
    )
    
    Write-Information "docker build $imageNames --force-rm --no-cache $buildArgs gitlab-runner-helper"

    & 'docker' build $imageNames --force-rm --no-cache $buildArgs gitlab-runner-helper
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to build docker image" )
    }
}

function Push-Tag($image)
{
    Write-Information "Push $image"

    & 'docker' push ${image}
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to push docker image $image" )
    }
}

function Connect-Registry($username, $password, $registry)
{
    Write-Information "Login registry $registry"

    & 'docker' login --username $username --password $password $registry
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to login $registry" )
    }
}

function Disconnect-Registry($registry)
{
    Write-Information "Logout registry $registry"

    & 'docker' logout $registry
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to logout from $registry" )
    }
}

Try
{
    Main
}
Finally
{
    if (-not (Test-Path env:SKIP_CLEANUP))
    {
        Write-Information "Cleaning up the build image"

        # We don't really care if these fail or not, clean up shouldn't fail
        # the pipelines.
        & 'docker' rmi -f "$Env:CI_REGISTRY_IMAGE/gitlab-runner-helper:$Env:WINDOWS_VERSION"
        & 'docker' rmi -f "${Env:DOCKERHUB_IMAGE}:$Env:WINDOWS_VERSION"
        & 'docker' image prune -f
    }
}
