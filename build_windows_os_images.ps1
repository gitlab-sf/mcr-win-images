Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"
$InformationPreference = "Continue"
# ---------------------------------------------------------------------------
# This script depends on a few environment variables that should be populated
# before running the script:
#
# - $Env:WINDOWS_VERSION - This is the version of windows that is going to be
#   used for building the Docker image. It is important for the version to match
#   one of the mcr.microsoft.com/powershell or https://hub.docker.com/_/microsoft-powershell
#   For example, `windowsservercore-20h2` will build from mcr.microsoft.com/powershell:windowsservercore-20h2.
function Main
{
    Build-Image $Env:WINDOWS_VERSION

    Connect-Registry "$Env:CI_REGISTRY_USER" "$Env:CI_REGISTRY_PASSWORD" "$Env:CI_REGISTRY"
    Push-Tag "$Env:CI_REGISTRY_IMAGE/powershell:$Env:WINDOWS_VERSION"
    Disconnect-Registry "$env:CI_REGISTRY"

    Connect-Registry "$Env:DOCKERHUB_USER" "$Env:DOCKERHUB_PASSWORD" "docker.io"
    Push-Tag "${Env:DOCKERHUB_WIN_IMAGE}:${Env:WINDOWS_VERSION}"
    Disconnect-Registry "docker.io"
}


function Build-Image($tag)
{
    Write-Information "Build image for $tag"

    $buildArgs = @(
        '--build-arg', "BASE_IMAGE_TAG=mcr.microsoft.com/powershell:$tag"
    )

    $imageNames = @(
        '-t', "${Env:CI_REGISTRY_IMAGE}/powershell:$tag",
        '-t', "${Env:DOCKERHUB_WIN_IMAGE}:$tag"
    )
    
    Write-Information "docker build $imageNames --force-rm --no-cache $buildArgs powershell"

    & 'docker' build $imageNames --force-rm --no-cache $buildArgs powershell
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to build docker image" )
    }
}

function Push-Tag($image)
{
    Write-Information "Push $image"

    & 'docker' push ${image}
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to push docker image $image" )
    }
}

function Connect-Registry($username, $password, $registry)
{
    Write-Information "Login registry $registry"

    & 'docker' login --username $username --password $password $registry
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to login $registry" )
    }
}

function Disconnect-Registry($registry)
{
    Write-Information "Logout registry $registry"

    & 'docker' logout $registry
    if ($LASTEXITCODE -ne 0) {
        throw ("Failed to logout from $registry" )
    }
}

Try
{
    Main
}
Finally
{
    if (-not (Test-Path env:SKIP_CLEANUP))
    {
        Write-Information "Cleaning up the build image"

        # We don't really care if these fail or not, clean up shouldn't fail
        # the pipelines.
        & 'docker' rmi -f "$Env:CI_REGISTRY_IMAGE/powershell:$Env:WINDOWS_VERSION"
        & 'docker' rmi -f "${Env:DOCKERHUB_WIN_IMAGE}:$Env:WINDOWS_VERSION"
        & 'docker' image prune -f
    }
}
